<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>'cors'], function(){
    Route::resource('/lapor','User\laporUserController');
    Route::resource('/melaporkan','User\melaporkanUserController');
    Route::get('/last','User\laporUserController@last');
    Route::post('/cari','User\laporUserController@search');
    Route::group(['middleware'=>'cors', 'prefix' => 'auth'], function() {
        Route::post('login','Admin\userAdminController@loginAdmin');
    });   
});

Route::group(['middleware'=>'cors', 'prefix' => 'admin'], function() {
        Route::resource('/lapor','Admin\laporAdminController');
        Route::get('myuser','Admin\userAdminController@getAdmin');
});

