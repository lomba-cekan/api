<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class MelaporkanUser extends Model
{
    public $table = 'lapor_user';
    public $fillable = [
        'lapor_id',
        'nama', 
        'jeniskelamin', 
        'email', 
        'nohp', 
        'kota',
        'bukti',
        'tindak_lanjuti'
    ];
}
