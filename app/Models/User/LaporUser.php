<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class LaporUser extends Model
{
    public $table = 'lapor_kehilangan';
    public $fillable = [
        'nama', 
        'jeniskelamin', 
        'email', 
        'nohp', 
        'platno', 
        'norangka'.
        'jeniskendaraan', 
        'kota', 
        'keterangantambahan', 
        'peristiwa', 
        'verifikasi',
        'bukti'
    ];
}
