<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Auth extends Model
{
    protected $table = 'admin';
    
    public $fillable = [
        'email',
        'password',
        'name'
    ];
}
