<?php

namespace App\Http\Controllers;
namespace App\Http\Controllers\Helper;

use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UploadImage extends Controller
{
    public function doUpload($file,$folder='vrf'){
	$name = time().'.'.$file->getClientOriginalExtension(); // Time is default name
	$path = public_path('upld/imgs/'.$folder);
	if(!is_dir($path)){
	File::MakeDirectory($path,0777);
	}
	$file->move($path,$name);
	return $name;
    }
}
