<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Auth as Auth;

class userAdminController extends Controller
{
    public function loginAdmin(Request $request)
    {
        $credentials = $request->only('email','password');

        try {
            if (! $token = \JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'data_salah'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }
    public function getAdmin($id)
    {
        // $clients = Auth::findOrFail(1)->get();
        // return json_encode($clients);
        return true;
        // return $clients->toArray();
    }
}
