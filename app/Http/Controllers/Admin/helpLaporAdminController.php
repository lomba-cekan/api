<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Lapor as Lapor;

class helpLaporAdminController extends Controller
{
    public function declineConfirmation($id){
        $client = Lapor::find($id);
        $client->verifikasi = '0';
        $client->save();
    }
}
