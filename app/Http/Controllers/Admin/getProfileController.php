<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\User as User;

class getProfileController extends Controller
{
    public function showProfile(){
        $id = JWTAuth::parseToken()->authenticate()->id;
        $clients = User::findOrFail($id);
        return $clients->toArray();
    }
}
