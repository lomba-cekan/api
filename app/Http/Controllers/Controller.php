<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function doUpload($file,$folder='vrf'){
	$name = time().'.'.$file->getClientOriginalExtension(); // Time is default name
	$path = public_path('upld/imgs/'.$folder);
	if(!is_dir($path)){
	File::MakeDirectory($path,0777);
	}
	$file->move($path,$name);
	return $name;
    }

	public function muUpload($file,$folder='mlprkan'){
	$name = time().'.'.$file->getClientOriginalExtension(); // Time is default name
	$path = public_path('upld/imgs/'.$folder);
	if(!is_dir($path)){
	File::MakeDirectory($path,0777);
	}
	$file->move($path,$name);
	return $name;
    }
}
