<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User\LaporUser as Lapor;

class laporUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Lapor::all();
        if(!$clients){
            return response()->toArray();
        }
        else{
            return $clients->toArray();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|min:3',
            'jeniskelamin' => 'required',
            'email' => 'required|email',
            'nohp' => 'required|numeric',
            'platno' => 'required',
            'jeniskendaraan' => 'required',
            'kota' => 'required',
            'bukti' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $clients = new Lapor;
        $clients->nama = $request->nama;
        $clients->jeniskelamin = $request->jeniskelamin;
        $clients->email = $request->email;
        $clients->nohp = $request->nohp;
        $clients->platno = $request->platno;
        $clients->norangka = $request->norangka;
        $clients->jeniskendaraan = $request->jeniskendaraan;
        $clients->kota = $request->kota;
        $clients->keterangantambahan = $request->keterangantambahan;
        $clients->peristiwa = $request->peristiwa;
        $clients->verifikasi = '0';
        /* Verifikasi Foto */
        if($request->hasFile('bukti') && $request->file('bukti')->isValid()){
            $title = $this->doUpload($request->file('bukti'));
            $clients->bukti = $title;
        }
        else{
            $clients->bukti = 'not verif';
        }
        $clients->save();
        return json_encode($clients);
        // if(!$execute){
        //     return response()->json('error', 500);
        // }
        // else{
        //     return response()->json('success', 201);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clients = Lapor::findOrFail($id)->where('verifikasi','1')->get();
        return $clients->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
    * 3 Rank # 
    * 
    * group by
    * count
    */
    public function last(){
        $clients = Lapor::where('verifikasi','1')->orderBy('id', 'desc')->limit(3)->get();
        return $clients->toArray();
    }
    public function search(Request $request){
        $clients = Lapor::where([
            ['platno','like','%'.$request->cari.'%'],
            ['verifikasi','1'],
        ])->get();
        return $clients->toArray();
    }
}
