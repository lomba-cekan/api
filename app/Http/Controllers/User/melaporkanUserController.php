<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User\MelaporkanUser as Melaporkan;

class melaporkanUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clients = new Melaporkan;
        $clients->lapor_id = $request->lapor_id;
        $clients->nama = $request->nama;
        $clients->jeniskelamin = $request->jeniskelamin;
        $clients->email = $request->email;
        $clients->nohp = $request->nohp;
        $clients->kota = $request->kota;
        $clients->tindak_lanjuti = '0';
        if($request->hasFile('bukti') && $request->file('bukti')->isValid()){
            $title = $this->muUpload($request->file('bukti'));
            $clients->bukti = $title;
        }
        else{
            $clients->bukti = 'not verif';
        }
        $clients->save();
        return json_encode($clients);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
