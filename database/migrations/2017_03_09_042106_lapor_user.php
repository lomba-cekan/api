<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LaporUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lapor_user', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('lapor_id')->unsigned();
            $table->foreign('lapor_id')->references('id')->on('lapor_kehilangan');
            $table->string('nama', 191);
            $table->integer('jeniskelamin')->comment('1 = Laki Laki;2 = Perempuan');
            $table->string('email');
            $table->string('nohp');
            $table->string('kota');
            $table->string('bukti');
            $table->integer('tindak_lanjuti')->comment('1 = Tindak Lanjuti; 0 = Belum TD');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
