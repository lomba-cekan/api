<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kehilangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lapor_kehilangan', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('nama', 191);
            $table->integer('jeniskelamin')->comment('1 = Laki Laki;2 = Perempuan');
            $table->string('email');
            $table->string('nohp');
            $table->string('platno')->unique();
            $table->string('norangka')->unique();
            $table->string('jeniskendaraan');
            $table->string('kota');
            $table->text('keterangantambahan')->nullable();
            $table->text('peristiwa')->nullable();
            $table->string('bukti');
            $table->integer('verifikasi')->comment('1 = Aktif; 0 = nonaktif');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
